// Fold-o-Matic is a stream transformer that transforms an incoming
// stream of lines of ASCII text (from Line-o-Matic) into another
// stream of lines, with any continuation lines present in the input
// stream (signified by leading whitespace) concatenated to their
// parent.

export const makeFoldOMatic = (
  residue?: string
): TransformStream<string, string> => {
  let _residue = residue ?? '';
  const _stream = new TransformStream<string, string>({
    transform: (chunk, controller) => {
      if (leadlingWhitespaceNonemptyRegex.test(chunk)) {
        _residue = _residue + chunk;
      } else {
        if (_residue) {
          controller.enqueue(_residue);
        }
        _residue = chunk;
      }
    },
    flush: (controller) => {
      if (_residue) {
        controller.enqueue(_residue);
      }
    },
  });
  return {
    readable: _stream.readable,
    writable: _stream.writable,
  };
};

const leadlingWhitespaceNonemptyRegex = /^\r\n\s+.*/;
