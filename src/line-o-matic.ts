// Line-o-Matic is a stream transformer that transforms an incoming
// stream of Uint8Array chunks, assumed to be in the format of an
// RFC-5322 message, into a stream of CRLF-delimited lines of ASCII
// text. Because of the pecularities of the MIME format definition,
// the CRLF character pairs are located at the *beginning* of each
// line of text, rather than at the end.
export const makeLineOMatic = (
  residue?: string
): TransformStream<Uint8Array, string> => {
  let _residue = residue ?? '\r\n';
  const _stream = new TransformStream<Uint8Array, string>({
    transform: (chunk, controller) => {
      const s = _residue + new TextDecoder().decode(chunk);
      const lines = s.match(crlfRegex);
      let charCount = 0;
      lines?.forEach((line) => {
        controller.enqueue(line);
        charCount += line.length;
      });
      _residue = s.substring(charCount);
    },
    flush: (controller) => {
      if (_residue) {
        controller.enqueue(_residue);
      }
    },
  });
  return {
    readable: _stream.readable,
    writable: _stream.writable,
  };
};

const crlfRegex = /\r\n.*/g;
