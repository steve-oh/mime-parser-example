export interface MimeNode {
  contentType: string;
  headers: ReadableStream<string>;
  content: ReadableStream<string> | ReadableStream<MimeNode>;
}

export interface MimeRec {
  headers: string[];
  content: (MimeRec | string)[];
}
