import { MimeNode } from './models';

export interface MimeOMatic extends TransformStream<string, MimeNode> {
  id: number;
}

export const makeMimeOMatic = (level = ''): MimeOMatic => {
  // randomly generated ID (for logging)

  const _id = Math.floor(Math.random() * 65536 * 65536);

  // context

  let _contentType = '';
  let _boundary: string | undefined = undefined;

  let _headers: TransformStream<string, string>;
  let _headersWriter: WritableStreamDefaultWriter<string>;

  let _content: TransformStream<string, string | MimeNode>;
  let _contentWriter: WritableStreamDefaultWriter<string | MimeNode>;

  // guards

  const isBoundary = (line: string) => line === `\r\n--${_boundary}`;

  const isContentTypeMultipartMime = (line: string) => {
    const matches = line.match(contentTypeRegex);
    return !!matches && matches[1] === 'multipart';
  };

  const isContentTypeMime = (line: string) => contentTypeRegex.test(line);

  const isEmptyLine = (line: string) => emptyLineRegex.test(line);

  const isEnd = (line: string) => line === `\r\n--${_boundary}--`;

  const isNotBoundaryOrEnd = (line: string) =>
    !isBoundary(line) && !isEnd(line);

  const isPartEnd = (line: string) => line === partEndMarker;

  // actions

  const appendBody = async (line: string) => {
    await _contentWriter.ready;
    await _contentWriter.write(line);
  };

  const appendHeader = async (line: string) => {
    await _headersWriter.ready;
    await _headersWriter.write(line);
  };

  const setBoundary = (line: string) => {
    const matches = line.match(contentTypeRegex);
    if (!!matches) {
      const initialAccum: { [key: string]: string } = {};
      const parameters = matches[3]
        .match(parametersRegex)
        ?.reduce((accum: { [key: string]: string }, parameter: string) => {
          const parameterParts = parameter.match(parameterRegex);
          const key: string | undefined =
            parameterParts?.[1] ?? parameterParts?.[3];
          const value: string | undefined =
            parameterParts?.[2] ?? parameterParts?.[4];
          if (!!key && !!value) {
            accum[key] = value;
          }
          return accum;
        }, initialAccum);
      _boundary = parameters?.boundary;
      return;
    }
    _boundary = undefined;
  };

  const setContentType = (line: string) => {
    const matches = line.match(contentTypeRegex);
    _contentType = !!matches ? `${matches[1]}/${matches[2]}` : '';
  };

  const writeNode = (controller: TransformStreamDefaultController) => {
    // console.log(`${level}[${_id.toString(16)}] write node`);
    _headersWriter.close();
    _contentWriter.close();
    controller.enqueue({
      contentType: _contentType,
      headers: _headers.readable,
      content: _content.readable,
    });
    initialize();
  };

  // misc

  const initialize = () => {
    _contentType = '';
    _headers = new TransformStream<string, string>(
      undefined,
      new CountQueuingStrategy({ highWaterMark: 65536 }),
      new CountQueuingStrategy({ highWaterMark: 65536 })
    );
    _headersWriter = _headers.writable.getWriter();
    _content = new TransformStream<string, MimeNode | string>(
      undefined,
      new CountQueuingStrategy({ highWaterMark: 65536 }),
      new CountQueuingStrategy({ highWaterMark: 65536 })
    );
    _contentWriter = _content.writable.getWriter();
  };

  const logState = () => {
    // console.log(`${level}[${_id.toString(16)}] enter state ${State[_state]}`);
  };

  // Here's where the magic happens.

  let _state = State.idle;
  initialize();
  const _stream = new TransformStream<string, MimeNode>(
    {
      transform: async (chunk, controller) => {
        switch (_state) {
          // The idle state occurs at startup, and the state machine
          // returns to it after having finished parsing a MIME node.
          case State.idle:
            // If we detect a multipart-MIME header, we save the
            // header info and the boundary marker, and set up a child-
            // level Mime-o-Matic to handle the individual sections
            // within this multipart node.
            if (isContentTypeMultipartMime(chunk)) {
              setContentType(chunk);
              appendHeader(chunk);
              setBoundary(chunk);
              _content = makeMimeOMatic(level + '  ');
              _contentWriter = _content.writable.getWriter();
              _state = State.inMultipartMimeHeader;
              logState();
              return;
            }
            // If we detect an ordinary (non-multipart) MIME header, we
            // save the header info and set up a next-level string
            // stream to handle the content of this MIME section. Note
            // that the guard here will also pass a multipart header,
            // so this guard *must* come after the multipart guard.
            if (isContentTypeMime(chunk)) {
              setContentType(chunk);
              appendHeader(chunk);
              _content = new TransformStream<string, string>(
                undefined,
                new CountQueuingStrategy({ highWaterMark: 65536 }),
                new CountQueuingStrategy({ highWaterMark: 65536 })
              );
              _contentWriter = _content.writable.getWriter();
              _state = State.inMimeHeader;
              logState();
              return;
            }
            // While in the idle state, we ignore anything that isn't a
            // MIME header, so any non-MIME parts of the incoming
            // message (assorted headers, for the most part) are
            // discarded.
            return;

          // We enter the inMultipartMimeHeader state upon receipt of a
          // multipart-MIME header, and we stay there as long as we
          // receive more headers. The end of the headers is signified
          // by receipt of a blank line.
          case State.inMultipartMimeHeader:
            // When we receive a blank line, it's time to move on....
            if (isEmptyLine(chunk)) {
              _state = State.inMultipartMimeWaitingForBoundary;
              logState();
              return;
            }
            // If we receive something that looks like a header, we
            // append it to the header stream.
            if (isNotBoundaryOrEnd(chunk)) {
              appendHeader(chunk);
              return;
            }
            // If we reach here, the message is malformed in some way,
            // and we should probably report an error (but in this
            // example code we do nothing: not our job).
            return;

          // We enter the inMultipartMimeWaitingForBoundary state once
          // all of the headers have been received. Here, we should
          // receive either a boundary marker, indicating that another
          // MIME section is up next, or an end marker, indicating that
          // this multipart-MIME block is done.
          case State.inMultipartMimeWaitingForBoundary:
            // If we receive a boundary marker, what comes next is a
            // MIME section body.
            if (isBoundary(chunk)) {
              _state = State.inMultipartMimeBody;
              logState();
              return;
            }
            // If we receive an end marker, we're done, so we write
            // ourselves to the output stream and go back to the idle
            // state.
            if (isEnd(chunk)) {
              writeNode(controller);
              _state = State.idle;
              logState();
              return;
            }
            // We should probably never reach here....
            return;

          // We enter the inMultipartMimeBody state once we've gotten
          // past all of the headers and markers and whatnot, and are
          // in a child MIME section. Here, we mostly pass the input
          // lines directly to the child Mime-o-Matic machine, unless
          // we/ encounter a boundary or end marker, in which case we
          // signal to the child that they're at the end of the line
          // and they need to wrap it up.
          case State.inMultipartMimeBody:
            // If we receive a boundary marker, we notify the child.
            if (isBoundary(chunk)) {
              await _contentWriter.ready;
              await _contentWriter.write(partEndMarker);
              return;
            }
            // If we receive an end marker, we notify the child, and
            // then write ourselves to the output stream and go back to
            // the idle state.
            if (isEnd(chunk)) {
              await _contentWriter.ready;
              await _contentWriter.write(partEndMarker);
              writeNode(controller);
              _state = State.idle;
              logState();
              return;
            }
            // Otherwise, we simply pass the line to the child.
            await _contentWriter?.ready;
            await _contentWriter?.write(chunk);
            return;

          // The remaining states apply to simple (non-multipart) MIME
          // sections.

          // We enter the inMimeHeader state upon receipt of a
          // non-multipart-MIME header, and we stay there as long as we
          // receive more headers. The end of the headers is signified
          // by receipt of a blank line.
          case State.inMimeHeader:
            // If we receive a blank line, it's time to move on to the
            // body of the section.
            if (isEmptyLine(chunk)) {
              _state = State.inMimeBody;
              logState();
              return;
            }
            // If we receive a part-end marker, our parent multipart
            // section is telling us that we're done, so we write
            // ourselves to the output stream and return to the idle
            // state.
            if (isPartEnd(chunk)) {
              writeNode(controller);
              _state = State.idle;
              logState();
              return;
            }
            // Otherwise, we've (probably) received a header, so we
            // add it to the list.
            await appendHeader(chunk);
            return;

          // We enter the inMimeBody state upon receipt of a blank line
          // signifying the end of the headers. At this point, we're
          // ready to receive the actual body of the MIME section.
          case State.inMimeBody:
            // If we receive a part-end marker, our parent multipart
            // section is telling us that we're done, so we write
            // ourselves to the output stream and return to the idle
            // state.
            if (isPartEnd(chunk)) {
              writeNode(controller);
              _state = State.idle;
              logState();
              return;
            }

            // Otherwise, we append the line to our content body
            // stream.
            await appendBody(chunk);
            return;
        }
      },
    },
    new CountQueuingStrategy({ highWaterMark: 65536 }),
    new CountQueuingStrategy({ highWaterMark: 65536 })
  );
  return {
    id: _id,
    readable: _stream.readable,
    writable: _stream.writable,
  };
};

enum State {
  idle,
  inMultipartMimeHeader,
  inMultipartMimeWaitingForBoundary,
  inMultipartMimeBody,
  inMimeHeader,
  inMimeBody,
}

const contentTypeRegex =
  /Content-Type:\s+(\w+)\/(\w+)((?:;\s+(?:\w+=[^<>@,;:\(\)\\"\/[]?=]+|\w+=".+"))*)/;
const emptyLineRegex = /^\r\n$/;
const parameterRegex = /;\s+(\w+)=([^<>@,;:\(\)\\"/[]?=]+)|(\w+)="(.+)"/;
const parametersRegex = /(;\s+(?:\w+=[^<>@,;:\(\)\\"/[]?=]+|\w+=".+"))/g;

const partEndMarker = '\u200Bend-end-end-end-end-end-end-end\u200B';
