import './styles/main.css';

import { MimeNode, MimeRec } from './models';

import { makeLineOMatic } from './line-o-matic';
import { makeFoldOMatic } from './fold-o-matic';
import { makeMimeOMatic } from './mime-o-matic';

declare global {
  interface Window {
    parse: (input: ReadableStream<Uint8Array>) => ReadableStream<MimeNode>;
    showMimeStream: (
      stream: ReadableStream<MimeNode>
    ) => Promise<(MimeRec | string)[]>;
  }
}

window.parse = (
  input: ReadableStream<Uint8Array>
): ReadableStream<MimeNode> => {
  const lineOMatic = makeLineOMatic();
  const foldOMatic = makeFoldOMatic();
  const mimeOMatic = makeMimeOMatic();

  input.pipeThrough(lineOMatic);
  lineOMatic.readable.pipeThrough(foldOMatic);
  foldOMatic.readable.pipeThrough(mimeOMatic);

  return mimeOMatic.readable;
};

window.showMimeStream = async (
  stream: ReadableStream<MimeNode>
): Promise<(MimeRec | string)[]> => {
  const res: (MimeRec | string)[] = [];
  const reader = stream.getReader();
  while (true) {
    const node = await reader.read();
    if (node.value) {
      res.push(await showContent(node.value));
    } else {
      break;
    }
  }
  return res;
};

const showContent = async (
  content: MimeNode | string,
  indent = ''
): Promise<MimeRec | string> => {
  if (typeof content === 'string') {
    return content;
  } else {
    const res: MimeRec = { headers: [], content: [] };
    const mimeNode = content as MimeNode;
    const headersReader = mimeNode.headers.getReader();
    while (true) {
      const header = await headersReader.read();
      if (header.value) {
        res.headers.push(header.value);
      } else {
        break;
      }
    }
    const contentReader = mimeNode.content.getReader();
    while (true) {
      const content = await contentReader.read();
      if (content.value) {
        res.content.push(await showContent(content.value, indent + '  '));
      } else {
        break;
      }
    }
    return res;
  }
};
