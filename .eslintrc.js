module.exports = {
  root: true,

  parserOptions: {
    parser: require.resolve('@typescript-eslint/parser'),
  },

  env: {
    browser: true,
    es2021: true,
    node: true,
  },

  extends: ['plugin:@typescript-eslint/recommended', 'prettier'],

  plugins: ['@typescript-eslint'],

  rules: {
    'prefer-promise-reject-errors': 'off',
    quotes: ['warn', 'single', { avoidEscape: true }],
    'no-unused-vars': 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
  },
};
