The code in this repository comprises a simple parser for internet
messages containing multipart-MIME sections, along with a test app.

To run, install the needed packages and build the app:

```
yarn install
yarn build
```

then start the app in dev mode:

```
yarn dev
```

Navigate your browser to `http://localhost:8080' to view the app.

### How it works

The parser has a three-stage pipeline:

- line-o-matic: This accepts an incoming stream of `Uint8Array` chunks
  and converts it into a stream of strings, one for each CRLF-delimited
  line in the input.
- fold-o-matic: This accepts the stream of strings and does a small bit
  of semantic processing to concatenate any headers that have been split
  across lines.
- mime-o-matic: This accepts the output of fold-o-matic and does the
  actual parsing into MIME nodes.

### Notes

There is one issue that I was not able to resolve. As best as I can
determine, if you want to have a `ReadableStream<T>`, where `T` is an
object that itself contains one or more `ReadableStream`'s, then you
can't emit the enclosing object until the inner stream has been fully
loaded. This would seem to defeat the purpose of stream processing, but
I haven't been able to find a way around it.

To illustrate, I will represent a MIME node as `{n ...}`, where `n` is
just an index to keep track of which node I'm talking about. A typical
email message containing one attachment, produced by, say, Outlook,
would look something like this (ignoring the non-MIME parts):

```
{1 {2 {3 text} {4 html}} {5 attachment}}
```

That is, there is an outermost multipart/mixed node (1), enclosing a
multipart/alternative node (2) that contains the body of the email in
both text (3) and HTML (4) format, followed by the attachment (5).

So, assuming the attachment body is represented in the node as a
stream, it appears impossible to emit the outer node (1) until after
the entire attachment has been read in to memory.

Even if we flatten the multipart portions, we're still faced with the
fact that a single MIME node with a large body (e.g., a large image or
video) cannot be emitted until the entire body has been read in.

There is one other, smaller issue: Because I decided to leave the MIME
bodies in line format, rather than chunking them back into larger
strings (or `Uint8Array`'s), the arbitrary limit of 65,536 chunks that
is currently hardcoded into Mime-o-Matic means that a MIME body is
limited to about 3.5 MB of data. This is easily remedied by increasing
the chunk side of the body content stream, which is left as an exercise
for the reader.
